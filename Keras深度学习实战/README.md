﻿# 《Keras深度学习实践》代码记录
- SimpleMnistNeuralNetwork.py 第一章，Keras搭建一个没有隐藏层的神经网络训练mnist数据集，得到92.22%的准确率。
- LeNetMnist.py 第3章，Keras搭建LeNet卷积神经网络，在Mnist数据集上，得到99.2%的准确率。
- NeuralNetworkCifarDataset.py 第3章，Keras搭建一个基础的深度网络用于CIFAR图像分类，准确率为66.4%。
- NeuralNetworkCifarDataset2.py 第3章，从网络深度上改进上一个网络用于CIFAR图像分类，准确率为76.9%。
- VGG16.py 第3章，构建VGG16网络结构，加载VGG16的模型，对猫的图片进行预测。
- adversarial_example_gan.py 第4章，使用Adversarial包构建GAN网络生成Mnist手写数字，注意Keras版本为2.2.0会报错。
- example_gan_cifar10.py 第4章，使用Adversarial包构建GAN网络生成Cifar图片，类似于adversarial_example_gan.py。
- skipgram_example.py 第5章，生成skip-gram word2vec模型。
- keras_cbow.py 第5章，生成CBOW word2vec模型。
- keras_skipgram.py 第5章，从模型中提取出word2vec向量。

