# 这是什么

这是我在学习一些深度学习框架例如Tensorflow，Keras，Pytorch以及做一些算法比赛题目例如Codeforces，BZOJ或者入门一些深度学习课题例如GAN和CS231N课程时所记录的一些代码实现，总结等等。具体包括：

- Codeforces 一个俄罗斯的程序设计竞赛网站，可以防止老年弱智。
- CS231N 学习CS231N课程的时候灵星记录了一些numpy实现的算法。
- GAN 学习生成对抗网络记录一些算法实现。
- Keras深度学习实战 《Keras深度学习实战》书中一些算法实现和记录。
- Tensorflow入门时记录的一些小例子 RT。
- TensorFlow实战Google深度学习框架 《TensorFlow实战Google深度学习框架》书中算法实现和记录。
- Tensorflow实战 《TensorFlow实战》书中算法实现和记录。
- 统计学习方法 李航《统计学习方法》书中算法实现和记录。
