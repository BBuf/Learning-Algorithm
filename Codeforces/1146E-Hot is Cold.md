# 题意

给出一个序列，有两种操作：>x将大于x的数全部取负，<x将小于x的数全部取负，最后输出序列中所有数。

# 解法

我们先考虑对于一个数来说，它最后的状态只取决于它初始的时候是哪个数，而跟它所处的位置无关。我们注意到序列中的数的范围是[−1e5, 1e5]，那么我们只需要处理出每个数在经过q次操作后的状态是什么，最后查表就好了。我们定义一个状态1和−1，分别表示一个数的当前状态是取负了还是没有取负。我们以>x这个操作为例：

- 如果x>0，那么也就是说[x+1,1e5]这部分的数的状态肯定是-1, 并且[−1e5, −x−1]这部分数的状态肯定是1，而不用管它们之前是什么状态，那么直接区间赋值就好了。
- 如果x<0，那么[x+1,−x−1]这部分数的状态是反转，[−x, 1e5]这部分数的状态肯定是−1，[−1e5, x]这部分数的状态肯定是1，那么区间反转就好了。
  用线段树维护区间赋值和区间反转就可以了，同理<x的操作也类似处理

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
const int maxn = 200010;
const int D = 100005;
int n, q, a[maxn];
struct node{
    int l, r, val, lazy[2];
    //区间赋值
    void add1(int v){
        lazy[1] = 1;
        lazy[0] = v;
        val = v;
    }
    //区间翻转
    void add2(int v){
        val *= v;
        if(lazy[0] != 0){
            lazy[0] *= v;
        }else{
            lazy[1] *= v;
        }
    }
}Tree[maxn<<2];

void build(int l, int r, int rt){
    Tree[rt].l = l;
    Tree[rt].r = r;
    Tree[rt].lazy[0] = 0;//区间赋值
    Tree[rt].lazy[1] = 1;//区间反转
    Tree[rt].val = 1;
    if(l == r){
        return;
    }
    int mid = (l + r) / 2;
    build(l, mid, rt * 2);
    build(mid + 1, r, rt * 2 + 1);
}

void pushdown(int rt){
    if(Tree[rt].lazy[1] != 1){
        Tree[rt*2].add2(Tree[rt].lazy[1]);
        Tree[rt*2+1].add2(Tree[rt].lazy[1]);
        Tree[rt].lazy[1] = 1;
    }
    if(Tree[rt].lazy[0]!=0){
        Tree[rt*2].add1(Tree[rt].lazy[0]);
        Tree[rt*2+1].add1(Tree[rt].lazy[0]);
        Tree[rt].lazy[0] = 0;
    }
}

void update1(int l, int r, int L, int R, int val, int rt){
    if(L <= l && r <= R){
        Tree[rt].add1(val);
        return;
    }
    int mid = (l+r)>>1;
    pushdown(rt);
    if(L <= mid) update1(l, mid, L, R, val, rt*2);
    if(mid < R) update1(mid+1, r, L, R, val, rt*2+1);
}

void update2(int l, int r, int L, int R, int val, int rt){
    if(L <= l && r <= R){
        Tree[rt].add2(val);
        return;
    }
    int mid = (l+r)>>1;
    pushdown(rt);
    if(L <= mid) update2(l, mid, L, R, val, rt*2);
    if(mid < R) update2(mid+1, r, L, R, val, rt*2+1);
}

int query(int l, int r, int rt, int pos){
    if(l == r){
        return Tree[rt].val;
    }
    int mid = (l + r) >> 1;
    pushdown(rt);
    if(pos <= mid) return query(l, mid, rt*2, pos);
    else return query(mid+1, r, rt*2+1, pos);
}

int main(){
    scanf("%d%d", &n, &q);
    for(int i=1; i<=n; i++) scanf("%d", &a[i]);
    build(1, 2*D, 1);
    //puts("success");
    char op[10];
    int x;
    while(q--){
        scanf("%s%d", op,&x);
        if(op[0] == '>'){
            if(x < 0){
                update2(1, 2*D, x+1+D, -x-1+D, -1, 1);
                update1(1, 2*D, -100000+D, x+D, 1, 1);
                update1(1, 2*D, -x+D, 100000+D, -1, 1);
            }
            else{
                update1(1, 2*D, x+1+D, 100000+D, -1, 1);
                update1(1, 2*D, -100000+D, -x-1+D, 1, 1);
            }
        }else{
            if(x > 0){
                update2(1, 2*D, -x+1+D, x-1+D, -1, 1);
                update1(1, 2*D, x+D, 100000+D, 1, 1);
                update1(1, 2*D, -100000+D, -x+D, -1, 1);
            }else{
                update1(1, 2*D, -100000+D, x-1+D, -1, 1);
                update1(1, 2*D, -x+1+D, 100000+D, 1, 1);
            }
        }
    }
    for(int i=1; i<=n; i++){
        printf("%d%c", a[i] * query(1, D*2, 1, a[i]+D), " \n"[i == n]);
    }
    return 0;
}
```