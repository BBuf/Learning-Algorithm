# 《Tensorflow实战Google深度学习框架》代码记录

- ForwardPropagationAlgorithms.py  第3章，Tensorflow通过变量实现神经网络的参数并实现前向传播的过程。
- Two-classification.py  第3章，在一个模拟数据集上训练神经网络解决2分类问题。
- PredictMerchandiseSales.py  第3章，预测商品销售，用于说明不同的损失函数选取会对训练得到的模型产生重要影响。
- FiveLayerNeuralNetworkWithL2.py  第4章，计算一个5层神经网络带L2正则化的损失函数。
- ExponentialMovingAverage.py  第4章，滑动平均例子。
- MnistDigitRecognize.py 第5章，使用第4章介绍的神经网络结构设计和训练优化的所有方法构建一个识别Mnist书写数据集的算法。
