# 题意

有一棵树，给定结点数n，在每个结点上的操作（max：表示该结点的number为其孩子结点中的最大值，min相反），结点2..n的父结点。叶子结点上定义的操作可忽略，叶子结点的number为1..num，且互不相同，num为叶子结点个数，求根节点的number的最大值。

# 解法

智商题，我这种辣鸡肯定是做不来的。先用vector数组存储每个结点的子节点，用dp[i]表示结点i的最大number是其所有叶子结点中的第dp[i]大的，并且dp[j]=1（j为所有叶子结点的编号）。然后对于非叶子结点，若其操作为max，dp[i]=min(dp[j])（j为i的直接孩子结点，因为求最大值，故是第x大中的x越小越好）;若操作为min，则dp[i]=sum(dp[j])（j为i的直接孩子结点），最终答案为num+1-dp[1]（num为叶子结点个数）。

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
const int maxn = 3e5+10;
int n, op[maxn];
int dp[maxn], leaf;
vector <int> v[maxn];

void dfs(int x){
    if(x>1&&v[x].empty()){
        dp[x] = 1;
        ++leaf;
        return;
    }
    if(op[x]) dp[x] = 0x3f3f3f3f;
    else dp[x] = 0;
    for(int i = 0; i < v[x].size(); i++){
        dfs(v[x][i]);
        if(op[x]){//max
            dp[x] = min(dp[x], dp[v[x][i]]);
        }else{
            dp[x] = dp[x] + dp[v[x][i]];
        }
    }
}

int main(){
    scanf("%d", &n);
    for(int i=1; i<=n; i++) scanf("%d", &op[i]);
    for(int i=2; i<=n; i++){
        int x;
        scanf("%d", &x);
        v[x].push_back(i);
    }
    leaf = 0;
    dfs(1);
    printf("%d\n", leaf+1-dp[1]);
    return 0;
}
```

