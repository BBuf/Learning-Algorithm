# 题意

给定一个数字n，构造一个长度为2n的括号序列，每个‘（ ’都能在其右边找到唯一对应的‘ ）’则为合法序列，根据所有合法情况建一棵树，现有一个边集S满足集合内所有边均无公共点，求该边集的最大值（对1e9+7取模）。

#  思路1

从前三个样例就可以发现，只要保证间隔的边都取就一定会得到最大的边集，那么接下来就是如何建树，直接dfs按树扫所有边集大小，但是这样会t，那么就记忆化它，开一个dp[1000][1000][2]的数组记录已遍历过的值，dp[i][j][k]表示已经取了i个左括号和j个右括号且最后一个括号节点的状态为k（k为1表示该节点已经在边集S中，k为0表示该节点还没取过）。

 # 代码1

```C++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int maxn = 1002;
const int mod = 1e9+7;
LL dp[maxn][maxn][2];
int n;

LL dfs(LL a, LL b, LL k){
    LL ans = 0;
    if(dp[a][b][k]) return dp[a][b][k];
    int kk = k;
    if(a < n){
        int tmp = k ? 0 : 1;
        ans += dfs(a+1, b, tmp) + tmp;
        ans %= mod;
        k = 1;
    }
    if(a > b){
        int tmp = k ? 0 : 1;
        ans += dfs(a, b+1, tmp) + tmp;
        ans %= mod;
    }
    dp[a][b][kk] = ans % mod;
    return ans%mod;
}

int main(){
    scanf("%d", &n);
    memset(dp, 0, sizeof(dp));
    LL ans = (1 + dfs(1, 0, 1)) % mod;
    printf("%lld\n", ans);
    return 0;
}
```

# 思路2

在合法的括号序列中，左括号数一定大于等于右括号数的，所以我们可以先定义平衡度为左括号数-右括号数。然后可以发现一个规律：在trie同一深度上的点，如果平衡度相同，那么他的子树完全一样。然后我们对于他们的状态都可以用同一个表示方法表示。对于树上的最大边独立，为们对每个点，可以看他的父亲节点有没有被选过，如果被选过了，那么该点与父亲的边就不能选，反之则选择数量加1。然后我们可以直接dp求解。dp[i][j] 表示在第i层平衡度为j的点的最大独立边数

# 代码2

```C++
#include <bits/stdc++.h>
using namespace std;
const int maxn = 3005;
const int mod = 1e9+7;
int dp[maxn][maxn];
bool vis[maxn][maxn];

int main(){
    int n;
    scanf("%d", &n);
    n *= 2;
    dp[0][0] = 0;
    vis[0][0] = true;
    for(int i = 1; i <= n; i++){
        for(int j = 0; j <= n; j++){
            int sum = 0;
            bool flag = false;
            if(j >= 1){
                sum = (sum % mod + dp[i-1][j-1] % mod) % mod;
                flag |= vis[i - 1][j - 1];
            }
            if(j + 1 <= i - 1){
                sum = (sum % mod + dp[i-1][j+1] % mod) % mod;
                flag |= vis[i - 1][j + 1];
             }
             if(flag){
                dp[i][j] = (sum % mod + 1) % mod;
                vis[i][j] = false;
             }
             else{
                dp[i][j] = sum % mod;
                vis[i][j] = true;
             }
        }
    }
    printf("%d\n", dp[n][0]);
    return 0;
}
```

