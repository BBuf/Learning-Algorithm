# 题意

给你一个序列，你可以对区间里的某个连续序列全部乘以X，然后求最大子段和。

# 思路

DP。d1表示以a[i]结尾的最大子段和，d2表示a[i]被乘以x且以a[i]结尾的最大子段和，d3表示a[i]没有被乘以x，但在a[i]之前有一个区间被乘以，以a[i]结尾且包含该区间的最大子段和。答案就是d1,d2,d3中的最大者。

# 代码

```C++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
int n, k;

int main(){
    scanf("%d%d", &n,&k);
    vector <LL> a(n), d1(n), d2(n), d3(n);
    for(int i=0; i<n; i++) scanf("%lld", &a[i]);
    LL ans = 0;
    for(int i=0; i<n; i++){
        if(i==0) d1[i]=a[i];
        else{
            if(d1[i-1]>0) d1[i]=d1[i-1]+a[i];
            else d1[i]=a[i];
        }
        ans = max(ans, d1[i]);
    }
    for(int i=0; i<n; i++){
        if(i==0) d2[i]=k*a[i];
        else{
            long long maxx = max(max(d2[i-1], d1[i-1]), 0LL);
            d2[i] = maxx + k*a[i];
        }
        ans = max(ans, d2[i]);
    }
    if(n>1){
        d3[1] = a[0]*k + a[1];
        ans = max(ans, d3[1]);
    }
    for(int i=2; i<n; i++){
        d3[i] = a[i] + max(d3[i-1], d2[i-1]);
        ans = max(ans, d3[i]);
    }
    printf("%lld\n", ans);
    return 0;
}
```

