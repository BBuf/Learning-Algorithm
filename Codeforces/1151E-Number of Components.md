# 题意

一条链每个点都有权值，定义f(l, r)为留下权值[l, r]的节点联通快数量，求所有区间的f(l, r)之和。

# 解法

直接以一个点可以做多少个节点的区间左端点讨论。如果a[i]>a[i-1]，那么就是(a[i] - a[i-1])$\times$ (n-a[i]+1)，如果a[i]<a[i-1]，那么就是(a[i-1]-a[i])$\times$a[i]。扫描一遍计数即可，复杂度O(n)。

# 代码

```C++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int maxn = 1e5+10;
int n, a[maxn];
LL ans = 0;
int main(){
    scanf("%d", &n);
    for(int i=1; i<=n; i++) scanf("%d", &a[i]);
    for(int i=1; i<=n; i++){
        if(a[i] > a[i-1]){
            ans += 1LL*(a[i]-a[i-1])*(n-a[i]+1);
        }else{
            ans += 1LL*(a[i-1]-a[i])*a[i];
        }
    }
    printf("%lld\n", ans);
    return 0;
}
```

