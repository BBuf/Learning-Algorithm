# 题意

给你两个数组，一个只有奇数（1，3，5……），一个只有偶数（2，4，6……），让你合并为一个数组（第一次从奇数组拿一个过来即为1；第二次从偶数组拿两个数进来即为2，4；第三次从奇数组拿四个数进来，即为3，5，7，9，类似后推。）假设这个序列最长为1e18，给定任意l和r，求l，r区间对mod取余的和。

# 解法

因为整个序列实际上最多62块(1+2+4+8+...+2^62)，所以利用分块的思想, 预处理出每一块的和, 再询问时用中间整块的和加上左右部分块的和。这种方法做起来简单且容易理解，还可以通过二进制表示找规律，不过稍微麻烦，请参考：https://www.cnblogs.com/zaq19970105/p/10750208.html 。

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int maxn = 62;
const LL mod = 1000000007;
const LL inv = (1 + mod) / 2;
LL a[maxn+1], st[maxn+1], sum[maxn+1];

int get_block_id(long long x){
    return lower_bound(a+1, a+1+maxn, x) - a;
}

LL get_L(LL x){
    return a[get_block_id(x)-1] + 1;
}

LL get_R(LL x){
    return a[get_block_id(x)];
}

LL get_num(LL x){
    LL n = x - get_L(x) + 1;
    return (st[get_block_id(x)] + (n - 1) * 2) % mod;
}

void init(){
    for(int i=1; i<=maxn; i++) a[i] = (1LL<<i) - 1;
    st[1] = 1;
    st[2] = 2;
    for(int i=3; i<=maxn; i++) st[i] = (st[i-2]+(1LL<<(i-2)))%mod;
    sum[1] = 1;
    sum[2] = 7;
    for(int i=3; i<=maxn; i++){
        LL n = (1LL<<i-1)%mod;
        //等差数列求和,n*a1+n*(n-1)*d/2
        sum[i] = (sum[i-1] + n*st[i]%mod + n*(n-1)%mod)%mod;
    }
}

LL get_Ans(LL l, LL r){
    LL ans = 0;
    if(get_block_id(l) == get_block_id(r)){
        ans = (get_num(r) + get_num(l)) % mod * (r%mod - l%mod + 1)%mod * inv % mod;
    }else{
        ans = (get_Ans(l, get_R(l)) + get_Ans(get_L(r), r) + sum[get_block_id(r) - 1] - sum[get_block_id(l)]) % mod;
    }
    return ans;
}


int main(){
    init();
    LL l, r;
    scanf("%lld%lld", &l, &r);
    LL ans = get_Ans(l, r);
    if(ans < 0) ans += mod;
    printf("%lld\n", ans);
    return 0;
}
```