# 题意

从n个数中选出2个数，使得这两个数的最小公倍数最小。

# 思路

- LCM(a，b) = a * b / gcd(a，b)；考虑枚举公因数d，假设n个数中具有因子d的数分别是x1，x2，x3，x4.....(x1 < x2 < x3 < x4 ......)。

- 如果x1 与 x2 的最大公因数就是d，那么lcm(x1, x2) = x1 * x2 / d，如果x4与x1的最大公因数也是d，那么x4 * x1 / d一定大于x1 * x2 / d，所以后面的就不用考虑了。

- 如果x1 与x2 的最大公因数不是d，那么lcm(x1,x2) 一定小于x1 * x2 / d，即使x4与x1的最大公因数是d，那么x4 * x1 / d一定大于lcm(x1，x2)，同样后面也不用考虑了

- 所以我们只需要得到每个d的倍数的前2项即可，时间复杂度(1e7*log(le7))

# 代码

```C++
#include <bits/stdc++.h>
using namespace std;
const int maxn = 1e7+7;
int a[maxn], pos[maxn];
int n, x, id1, id2;

int main(){
    scanf("%d", &n);
    long long res = 1e18;
    for(int i=1; i<=n; i++){
        scanf("%d", &x);
        a[x]++;
        if(a[x]>1 && x<res){
            res = x;
            id1 = pos[x];
            id2 = i;
        }
        pos[x] = i;
    }
    for(int i=1; i<maxn; i++){
        int pre = 0;
        for(int j=i; j<maxn; j+=i){
            if(!a[j]) continue;
            if(!pre) pre = j;
            else{
                long long g = __gcd(pre/i, j/i);
                if(g == 1){
                    long long lcm = 1LL*j/i*pre;
                    if(lcm < res){
                        res = lcm;
                        id1 = pos[j];
                        id2 = pos[pre];
                    }
                }
                break;
            }
        }
    }
    if(id1 > id2) swap(id1, id2);
    printf("%d %d\n", id1, id2);
    return 0;
}
```

