#coding=utf-8
# tensorflow中集合的运用：损失集合
# 计算一个5层神经网络带L2正则化的损失函数
import tensorflow as tf
from numpy.random import RandomState

# 获得上一层神经网络边上的权重，并将这个权重的L2正则损失加入名称为'Losses'的集合中
def get_weights(shape, lamada):
    # 生成对应一层的权重变量
    var = tf.Variable(tf.random_normal(shape), dtype=tf.float32)
    # add_to_collection函数将这个新生成变量的L2正则化损失项加入集合
    tf.add_to_collection('losses', tf.contrib.layers.l2_regularizer(lamada)(var))
    #返回生成的变量
    return var

x = tf.placeholder(tf.float32, shape=(None, 2))
y_ = tf.placeholder(tf.float32, shape=(None, 1))
batch_size = 4
# 定义每一层网络中节点的个数
layer_dimension = [2, 10, 10, 10, 1]
# 神经网络的层数
n_layers = len(layer_dimension)
# 这个变量表示前向传播时最深层的节点，最开始的时候是输入层
cur_layer = x
# 当前层的节点个数
in_dimension = layer_dimension[0]
# 通过一个循环来生成5层全连接的神经网络结构
for i in range(1, n_layers):
    # layer_dimension[i]为下一层的节点个数
    out_dimension = layer_dimension[i]
    # 生成当前层中权重的变量，并将这个变量的L2正则化损失加入计算图中的集合
    weight = get_weights([in_dimension, out_dimension], 0.001)
    bias = tf.Variable(tf.constant(0.1, shape=[out_dimension]))
    # 使用ReLU激活函数
    cur_layer = tf.nn.relu(tf.matmul(cur_layer, weight) + bias)
    # 进入下一层之前将下一层的节点个数更新为当前层节点个数
    in_dimension = layer_dimension[i]
# 在定义神经网络前向传播的同时已经将所有的L2正则化损失加入了图上的集合
# 这里只需要计算刻画模型在训练数据集上表现的损失函数
mse_loss = tf.reduce_mean(tf.square(y_ - cur_layer))
# 将军方误差损失函数加入损失集合
tf.add_to_collection('losses', mse_loss)
# get_collection返回一个列表，这个列表是所有这个集合中的元素。在这个样例中
# 这些元素就是损失函数的不同部分，将它们加起来就可以得到最终的损失函数
loss = tf.add_n(tf.get_collection('losses'))

global_step = tf.Variable(0)
# 学习率的设置：指数衰减法
# 参数：初始参数，全局步骤，每训练100轮乘以衰减速度0.96（当staircase = True的时候）
learning_rate = tf.train.exponential_decay(0.1, global_step, 100, 0.96, staircase=True)
train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

rdm = RandomState()
dataset_size = 128
X = rdm.rand(dataset_size, 2)
# 加入了一个噪声值，-0.05~0.05之间
Y = [[x1 + x2 + rdm.rand() / 10.00 - 0.05] for (x1, x2) in X]

with tf.Session() as sess:
    init_op = tf.initialize_all_variables()
    sess.run(init_op)
    steps = 5000
    for i in range(steps):
        st = (i * batch_size) % dataset_size
        en = min(st + batch_size, dataset_size)
        sess.run(train_step, feed_dict={x: X[st:en], y_:Y[st:en]})
        if i % 100 == 0:
            total_loss = sess.run(loss, feed_dict={x: X, y_: Y})
            print("After %d training_steps, loss on all data is %g" % (i, total_loss))
