# 经典GAN的实现

- DCGAN.py 实现了DCGAN。数据集格式为任意创建一个文件夹，文件夹放一个人或者物体的图片，然后修改python脚本里面的数据加载和保存的路径即可，可以自定义batchsize和迭代次数。算法原理请看：https://blog.csdn.net/just_sort/article/details/84581400
- CycleGAN.py 实现了CycleGAN，用于数据风格迁移，参考了https://github.com/hardikbansal/CycleGAN ,数据集可到这个地址进行下载。算法原理请看：https://zhuanlan.zhihu.com/p/45077389
- CGAN.py 实现了CGAN用于生成某类Minist手写数字（条件GAN）。原理请看：https://blog.csdn.net/just_sort/article/details/84585471
