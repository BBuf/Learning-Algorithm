# CS231N作业
- k_nearest_neighbor.py 定义了KNearestNeightbor类，实现了KNN的训练，预测，3种方法计算L2距离。
- knn.py 实现了KNearestNeightbor类的调用，以及交叉验证，可视化等等。
- softmax.py 分别暴力和利用numpy实现了softmax的Loss和梯度计算。
- linear_svm.py 分别暴力和利用numpy实现了svm的Loss和梯度的计算。
- linear_classifier.py 定义使用svm的线性分类器的训练，预测。
- svm.py 调用linear_classifier对ciafr10数据集进行线性分类，以及交叉验证超参数选择，可视化特征图等。
- neural_net.py 实现了2层的神经网络的loss,梯度，以及训练和测试过程。
- two_layer_net.py 实现了neural_net的完整测试以及交叉验证获得最佳隐藏层，学习率，正则话系数的模型并可视化。在CIAFR10的准确率为50%。

