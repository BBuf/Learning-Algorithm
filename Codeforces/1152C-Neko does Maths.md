# 题意

给你$a$和$b$ ，找到一个最小的$k$，使得$lcm(a+k,b+k)$最小。

# 思路

不妨设$a>b$,则有：

$lcm(a+k, b+k)=\frac{(a+k)(b+k)}{gcd(a+k,b+k)}=\frac{a*b+(a+b)*k+k^2}{gcd(b+k,a-b)}$

枚举a-b的所有因子即可。

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
LL a, b;

//find minimal k, st. b * k >= a, then return b * k
LL get(LL a, LL b){
    if(a <= b) return b;
    if(a % b == 0) return a;
    return (a + b - a % b);
}
vector <int> v;

int main(){
    scanf("%lld%lld", &a, &b);
    if(a == b){
        return 0*puts("0");
    }
    if(a < b) swap(a, b);
    LL ans = a * b / __gcd(a, b);
    LL ans_k = 0;
    LL up = get(b, a - b);
    for(LL i = 1; i * i <= up; i++){
        if(up %i == 0){
            v.push_back(i);
            v.push_back(up / i);
        }
    }
    for(auto it : v){
        LL k = get(b, it) - b;
        LL tmp = (a * b + (a + b) * k + k * k) / it;
        if(tmp < ans){
            ans = tmp;
            ans_k = k;
        }
    }
    printf("%lld\n", ans_k);
    return 0;
}
```

