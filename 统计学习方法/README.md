# 统计学习方法

- Iris_perceptron.py  第2章，感知机模型的numpy，sklearn实现。
- knn_kdt.py 第3章，K近邻算法numpy，sklearn实现，KDTree实现。
- GaussianNativeBayes.py 第4章，朴素贝叶斯算法numpy，sklearn实现。
- DesionTree.py 第5章，决策树算法numpy，sklearn实现。
- DesionTree(ID3剪枝) 第5章，决策树使用ID3算法和剪枝技巧的numpy实现。
- Logistic.py 第6章，逻辑斯蒂回归的numpy, sklearn实现。