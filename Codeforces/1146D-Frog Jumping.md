# 题意

一只青蛙最开始在坐标0点，现在给了3个参数a，b，m。青蛙可以从k坐标点，向右调到k+a或者向左调到k-b坐标点。定义f(x)为青蛙在跳跃点不超过[0, x]区间内可以跳到的坐标点的个数，现在要求：
$$
\sum_{i=0}^{m}f(i)
$$


# 解法

首先下结论：如果x > a + b, 如果 gcd(a, b) ∣ x ，那么肯定可以在 f(x) 是x就可达了，那么x的贡献就是(m-x+1)。如果 x &lt; a + b, 我们可以暴力算出f(x) ，因为a,b的范围很小。我们不考虑f(x)中不能超过x的限制，每一个d的倍数的贡献都是(n - i * d + 1)。所有d的倍数的贡献就是:
$$
\sum_{i=1}^{i=m/d}(m - i \times d + 1)
$$
这是一个等差数列，可以直接O(1)求和。关键点在于：x能被表示，x-a 一定要能被表示；根据欧几里得扩展定理， gcd(a, b) |x 这个条件肯定需要被满足；

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;

const int maxn = 3e5+10;
bool vis[maxn];
int ans = 0;
LL m, a, b;
void bfs(int x){
    if(vis[x]) return;
    vis[x] = 1;
    ans++;
    queue <int> q;
    q.push(x);
    while(!q.empty()){
        int now = q.front();
        q.pop();
        if(now + a < x && !vis[now + a]){
            vis[now + a] = 1;
            q.push(now + a);
            ans++;
        }
        if(now - b > 0 && !vis[now - b]){
            vis[now - b] = 1;
            q.push(now - b);
            ans++;
        }
    }
}

LL getAns(LL n){
    LL d = __gcd(a, b);
    return n / d * (m + 1) - (n / d) * (n / d + 1) / 2 * d;
}

int main(){
    scanf("%lld%lld%lld", &m,&a,&b);
    LL cnt = 0;
    vis[0] = 1;
    ans = 1;
    for(int i = 0; i <= min(a + b, m); i++){
        if(i - a >= 0 && vis[i - a]){
            bfs(i);
        }
        cnt += ans;
    }
    LL Ans = cnt;
    if(m > a + b){
        Ans += getAns(m);
        Ans += (m - (a + b)) * ans;
        Ans -= getAns(a + b);
    }
    printf("%lld\n", Ans);
    return 0;
}
```

