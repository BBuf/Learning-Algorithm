#coding=utf-8
#人脸识别
from time import time
import logging
import matplotlib.pyplot  as plt

from sklearn.cross_validation import train_test_split
from sklearn.datasets import fetch_lfw_people
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report #sklearn.metrics模块实现了一些loss, score以及一些工具函数来计算分类性能
from sklearn.metrics import confusion_matrix #混淆矩阵
from sklearn.decomposition import RandomizedPCA
from sklearn.svm import SVC

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)%(message)s')
# 下载数据并以numpy array的形式载入
lfw_people = fetch_lfw_people(min_faces_per_person=70, resize=0.4)
# 获得数据的shape
n_samples, h, w = lfw_people.images.shape
X = lfw_people.data
n_features = X.shape[1] #特征值
#获取数据的lable
y = lfw_people.target
target_names = lfw_people.target_names
n_classes = target_names.shape[0]

print("Total dataset size:")
print("n_samples: %d" % n_samples)
print("n_features: %d" % n_features)
print("n_classes: %d" % n_classes)

#将数据集划分成训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
#利用PCA降维
n_components = 150
print("Extracting the top %d eigenfaces from %d faces" %
      (n_components, X_train.shape[0]))
t0 = time()
#whiten=True,代表是否进行归一化
pca = RandomizedPCA(n_components=n_components, whiten=True).fit(X_train)
print("done in %0.3fs" % (time() - t0))
eigenfaces = pca.components_.reshape((n_components, h, w)) #特征脸
t0 = time()
X_train_pca = pca.transform(X_train) #将数据X_train转换成降维后的数据。当模型训练好后，对于新输入的数据，都可以用transform方法来降维。
X_test_pca = pca.transform(X_test) #将数据X_test转换成降维后的数据。当模型训练好后，对于新输入的数据，都可以用transform方法来降维。
print("done in %0.3fs" % (time() - t0))

#训练SVM分类模型
t0 = time()
param_grid = {'C':[1e3, 5e3, 1e4, 5e4, 1e5], 'gamma':[0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1]}
clf = GridSearchCV(SVC(kernel='rbf', class_weight='balanced'), param_grid)
clf = clf.fit(X_train_pca, y_train)
print("done in %0.3fs" % (time() - t0))
print("Best estimator found by grid search:")
print(clf.best_estimator_)

#测试集模型质量的定量评价
t0 = time()
y_pred = clf.predict(X_test_pca)
print("done in %0.3fs" % (time() - t0))
#查准率/查全率/F1值/测试样本数
print(classification_report(y_test, y_pred, target_names=target_names))
print(confusion_matrix(y_test, y_pred, labels=range(n_classes)))

# Qualitative evaluation of the predictions using matplotlib

def plot_gallery(images, titles, h, w, n_row=3, n_col=4):
    """Helper function to plot a gallery of portraits"""
    plt.figure(figsize=(1.8 * n_col, 2.4 * n_row))
    plt.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_row * n_col):
        plt.subplot(n_row, n_col, i + 1)
        plt.imshow(images[i].reshape((h, w)), cmap=plt.cm.gray)
        plt.title(titles[i], size=12)
        plt.xticks(())
        plt.yticks(())


# plot the result of the prediction on a portion of the test set

def title(y_pred, y_test, target_names, i):
    pred_name = target_names[y_pred[i]].rsplit(' ', 1)[-1]
    true_name = target_names[y_test[i]].rsplit(' ', 1)[-1]
    return 'predicted: %s\ntrue:      %s' % (pred_name, true_name)

prediction_titles = [title(y_pred, y_test, target_names, i)
                     for i in range(y_pred.shape[0])]

plot_gallery(X_test, prediction_titles, h, w)

# plot the gallery of the most significative eigenfaces

eigenface_titles = ["eigenface %d" % i for i in range(eigenfaces.shape[0])]
plot_gallery(eigenfaces, eigenface_titles, h, w)

plt.show()



