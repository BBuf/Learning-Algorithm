import keras
from keras.models import Model
from keras.layers import Conv2D,UpSampling2D,MaxPooling2D
from keras.layers import Input
from keras.layers import LeakyReLU
from keras.layers import BatchNormalization
from keras import regularizers

print(keras.__version__)

def segnet(n_classes, input_height=64, input_width=64):
    img_input = Input(shape=(input_height, input_width, 3))
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(inp)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)
    enc = Conv2D(128, (3, 3), padding='same', activation='relu')(enc)

    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(enc)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)
    dec = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(dec)

    auto = Model(img_input, dec)
    return auto

