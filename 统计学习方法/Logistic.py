#coding=utf-8
from math import exp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.cross_validation import train_test_split

# data
def creat_data():
    iris = load_iris()
    df = pd.DataFrame(iris.data, columns=iris.feature_names)
    df['label'] = iris.target
    df.columns = ['sepal length', 'sepal width', 'petal length', 'petal width', 'label']
    data = np.array(df.iloc[:100, [0,1,-1]])
    return data[:, :2], data[:, -1]

X, y = creat_data()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

# 定义Logsitic模型
class LogisticReressionClassifier:
    def __init__(self, max_iter=200, learning_rate=0.01):
        self.max_iter = max_iter
        self.learning_rate = learning_rate
    def sigmoid(self, x):
        return 1 / (1 + exp(-x))
    def data_matrix(self, X):
        data_mat = []
        #1.0 相当于加了bias
        for d in X:
            data_mat.append([1.0, *d])
        return data_mat
    def fit(self, X, y):
        data_mat = self.data_matrix(X) #m*n
        #print(len(data_mat[0]))
        self.weights = np.zeros((len(data_mat[0]), 1), dtype=np.float32)
        for iter_ in range(self.max_iter):
            for i in range(len(X)):
                result = self.sigmoid(np.dot(data_mat[i], self.weights))
                error = y[i] - result
                self.weights += self.learning_rate * error * np.transpose([data_mat[i]])
            print('LogisticRegression Model(learning_rate={},max_iter={})'.
                  format(self.learning_rate, self.max_iter))
    def score(self, X_test, y_test):
        right = 0
        X_test = self.data_matrix(X_test)
        for x, y in zip(X_test, y_test):
            result = np.dot(x, self.weights)
            if (result >0 and y == 1) or (result < 0 and y == 0):
                right += 1
        return right / len(X_test)

lr_clf = LogisticReressionClassifier()
lr_clf.fit(X_train, y_train)
lr_clf.score(X_test, y_test)

x_points = np.arange(4, 8)
y_ = -(lr_clf.weights[1]*x_points + lr_clf.weights[0])/lr_clf.weights[2] #求y
plt.plot(x_points, y_)

plt.scatter(X[:50,0],X[:50,1], color='blue', label='0')
plt.scatter(X[50:,0],X[50:,1], color='red', label='1')
plt.legend()
plt.show()

#solver参数决定了我们对逻辑回归损失函数的优化方法，有四种算法可以选择，分别是：
#    a) liblinear：使用了开源的liblinear库实现，内部使用了坐标轴下降法来迭代优化损失函数。
#    b) lbfgs：拟牛顿法的一种，利用损失函数二阶导数矩阵即海森矩阵来迭代优化损失函数。
#    c) newton-cg：也是牛顿法家族的一种，利用损失函数二阶导数矩阵即海森矩阵来迭代优化损失函数。
#    d) sag：即随机平均梯度下降，是梯度下降法的变种，和普通梯度下降法的区别是每次迭代仅仅用一部分的样本来计算梯度，适合于样本数据多的时候。
from sklearn.linear_model import LogisticRegression
clf = LogisticRegression(max_iter=200)
clf.fit(X_train, y_train)
clf.score(X_test, y_test)
print(clf.coef_, clf.intercept_)


x_ponits = np.arange(4, 8)
y_ = -(clf.coef_[0][0]*x_ponits + clf.intercept_)/clf.coef_[0][1]
plt.plot(x_ponits, y_)

plt.plot(X[:50, 0], X[:50, 1], 'bo', color='blue', label='0')
plt.plot(X[50:, 0], X[50:, 1], 'bo', color='red', label='1')
plt.xlabel('sepal length')
plt.ylabel('sepal width')
plt.legend()
plt.show()

