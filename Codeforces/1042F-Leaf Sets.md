# 题意

给出一棵树，要求将其叶子分成几个集合，要求每个集合中叶子两两之间的距离不大于k，求至少要将其分成几个集合。

# 解法

对于每个点维护最深的叶子离自己的距离，向上合并时，将所有儿子按最深的叶子离自己的距离排序，从深到浅遍历尝试合并，如果相邻的两个距离加起来大于k，便无法合并，向上传也没有意义，于是就将该儿子及其子树单独分成一个集合，答案+1，最后返回在删去所有分离的儿子后最深的叶节点离自己的距离。

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
const int maxn = 1e6+5;
vector <int> G[maxn];
int ans, n, k;
int dfs(int x, int fa){
    if(G[x].size() == 1) return 0;
    vector <int> tmp;
    for(int i=G[x].size()-1; i>=0; i--){
        if(G[x][i] != fa){
            tmp.push_back(dfs(G[x][i], x) + 1);
        }
    }
    sort(tmp.begin(), tmp.end());
    for(int i=tmp.size(); i>1; i--){
        if(tmp[i-1] + tmp[i-2] > k){
            ans++;
            tmp.pop_back();
        }else{
            break;
        }
    }
    return tmp.back();
}
int main(){
    scanf("%d%d", &n,&k);
    int x, y;
    for(int i=1; i<n; i++){
        scanf("%d%d", &x,&y);
        G[x].push_back(y);
        G[y].push_back(x);
    }
    ans = 0;
    for(int i=1; i<=n; i++){
        if(G[i].size() > 1){
            dfs(i, 0);
            printf("%d\n", ans+1);
            exit(0);
        }
    }
    return 0;
}
```

