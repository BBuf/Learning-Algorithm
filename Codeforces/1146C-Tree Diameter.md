# 题意

有一个n个点的树，但我们不知道树的形态，需要求树的直径，你可以进行不大于10次询问，每次询问包含两个集合 x,y ,(x 与y交集为空), 之后会给出x集合中的点到y集合中的点最远的距离。


# 解法

本题的关键就是划分集合，你需要进行的10次询问必须将任意两个点的距离都清楚，这时候有趣的事情发生了，我们知道不同的树，二进制表达肯定不相同，所以我们可以将节点x按照二进制上的每一位是否为1划分两个集合，巧妙地利用二进制的特点。

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
int v1[105], v2[105];

int main(){
    int T, n;
    scanf("%d", &T);
    while(T--){
        int ans = 0;
        scanf("%d", &n);
        for(int i = 0; i <= 6; i++){
            int id1 = 0;
            int id2 = 0;
            for(int j = 1; j <= n; j++){
                if(j & (1<<i)) v1[++id1] = j;
                else v2[++id2] = j;
            }
            if(id1 &&  id2){
                printf("%d %d ", id1, id2);
                for(int i = 1; i <= id1; i++) printf("%d ", v1[i]);
                for(int i = 1; i <= id2; i++) printf("%d ", v2[i]);
                puts("");
                fflush(stdout);
                int x;
                scanf("%d", &x);
                if(x > ans){
                    ans = x;
                }
            }
        }
        printf("-1 %d\n", ans);
        fflush(stdout);
    }
    return 0;
}
```