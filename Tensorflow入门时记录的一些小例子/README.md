# 学习Tensorflow过程中的一些基础例子
   本工程记录一些学习Tensorflow过程中的一些常见网络实现，大多数是基于mnist数据集进行训练，也有部分是机器学习算法的实现。还有一个验证码识别的小项目captcha recognize demo，以及如何保存Tensorflow训练过程的参数的代码。
# 需要的配置环境
  python2 或者 python3 , tensorflow 1.0 ， capcha， matplotlib等等。
# 工程文件介绍
- CNN.py 使用tensorflow对Mnist数据集进行预测。 学习视频链接：https://www.bilibili.com/video/av29663946/?p=1
- KNN.py 针对IRIS数据集手动实现了K-Neighbor Nearest(KNN)算法。
- Logistic.py 利用逻辑史蒂回归对Mnist数据集进行预测。
- ModelSaveCNN.py 利用Minist数据集和tensorflow进行神经网络训练模型的保，以及取出保存的模型进行预测。
- ModelSaveExample1.py 在tensorflow中如何保存模型的基础例子。
- Neural Network.py 构建了2个隐藏层的简单神经网络对Mnist数据集进行预测。
- RNN.py 构造循环神经网络对mnist数据集进行预测。
- SVM-visualization.py 利用sklearn构造SVM模型，随机产生数据并画出超平面和支持向量的图。
- SVM-FaceRecognize.py 利用SVM和PCA降维在LFW数据集上实现了人脸识别。
- VGG.py 对VGG网络进行解析，并用猫的图片进行输出观察经过VGG网络的每一层后的feature map。
- captcha recognize demo.py 利用CNN识别python随机产生的验证码。
- linear regression.py 随机生成数据，实现线性回归模型。
- marsggbo-ensemble.py Kaggle 房价预测Top5%，使用了stacking模型融合。
- mobilenet.py 实现mobilenetv1的网络结构，针对mnist进行训练及测试。
- stacked hourglass.py 实现stacked hourglass的网络结构，并用mnist进行训练和测试。
- titanic 80%.py Kaggle上的Titanic生存预测比赛，80%准确率，使用了模型融合。
# 学习文章，博客
- 机器学习中的PR曲线和ROC曲线：https://blog.csdn.net/mingtian715/article/details/53488094
