# 解题记录

- 1042F-Leaf Sets 将一棵树的叶子节点合并为几个集合，且集合中点距不超过k。贪心。
- 1146C-Tree Diameter 交互题，不超过10次查询两个集合距离求树直径。二进制分组模拟。
- 1146D-Frog Jumping  暴力+统计贡献。
- 1146E-Hot is Cold 两种操作，>x将大于x的数全部取负，<x将小于x的数全部取负，求最终序列。线段树。
- 1151C-Problem for Nazar 1，2，4，3，5，7，9...序列求任意区间和。分块模型。
- 1151E-Number of Components 一条链每个点都有权值，定义f(l, r)为留下权值[l, r]的节点联通快数量，求所有区间的f(l, r)之和。统计每个点作为左端点贡献。
- 1151F-Sonya and Informatics DP+矩阵快速幂转移。
- 1152C-Neko does Maths.md 给你a和b，求一个k使得lcm(a+k,b+k)最小，展开lcm的等式，枚举a-b的约数即可。
- 1152D-Neko and Aki's Prank.md Trie数上的最大匹配，找规律加DP。
- 1153D-Serval and Rooted Tree.md 一道奇妙的思维题，DP。
- 1154G-Minimum Possible LCM.md 从n个数中选出2个数，使得这两个数的最小公倍数最小。
- 1155D-Beautiful Array.md 最大子段和的一个变形，可以把某个区间乘x，分3段进行DP。
- 1156C-Match Points.md 长度为n的数组，选择两个数，要求|差值| >= k，但是被选择过的数不允许被再次选择，问最大的匹配对数。贪心或者二分。