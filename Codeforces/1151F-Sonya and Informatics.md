# 题意

 给定一个01串，每次取出一组(l, r)，1<=l<r<=n，交换s[l],s[r]，求经过k次之后变成升序的概率多大？

# 思路

设n个数中0的个数为a,1的个数为b，用$d_{i,j}$表示完成i次操作后，能使前a个数字中有j个1的操作序列的数量，设原数组中，前a个数字里有c个0，则我们将$d_{0c}$初始化为1，问题的答案为$\frac{d_{k,a}}{\sum_{i=0}^ad_{k, i}}$,现在考虑一下转移,$d_{i,j}$可以从$d_{i-1,j-1}$，$d_{i-1,j}$，$d_{i-1,j+1}$转移而来：

- $d_{i-1,j}$:或者前a个数中的两个交换，或者后b个数中的两个交换，或者前a个数中的0与后b个数中的0交换，或者前a个数中的1与后b个数中的1交换。

- $d_{i-1,j-1}$前a个数中的1与后b个数中的0交换。

- $d_{i-1,j+1}$前a个数种的0与后b个数中的1交换。

  所以转移方程可以写为：

  $d_{i,j} = d_{i-1,j}*(C(a,2)+C(b,2)+j*(a-j)+(a-j)*(b-a+j)) +$ 

  $d_{i-1,j-1}*(a-j+1)*(a-j+1)+d_{i-1,j+1}*(j+1)*(b-a+j+1)$

   

# 代码

```c++
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const LL mod = 1e9+7;
const int maxn = 105;

struct Matrix{
    LL a[maxn][maxn];
    void init(){
        for(int i = 0; i < maxn; i++)
            for(int j = 0; j < maxn; j++)
                a[i][j] = 0;
    }
    void init2(){
        for(int i = 0; i < maxn; i++){
            for(int j = 0; j < maxn; j++){
                if(i == j) a[i][j] = 1;
                else a[i][j] = 0;
            }
        }
    }
};

Matrix mul(Matrix a, Matrix b){
    Matrix res;
    res.init();
    for(int i = 0; i < maxn; i++){
        for(int j = 0; j < maxn; j++){
            for(int k = 0; k < maxn; k++){
                res.a[i][j] = (res.a[i][j] + a.a[i][k] * b.a[k][j] % mod) % mod;
            }
        }
    }
    return res;
}

Matrix qpow(Matrix a, int n){
    Matrix res;
    res.init2();
    while(n){
        if(n&1) res = mul(res, a);
        a = mul(a, a);
        n >>= 1;
    }
    return res;
}

LL qsm(LL a, LL n){
    LL ret = 1;
    while(n){
        if(n&1) ret = ret * a % mod;
        a = a * a % mod;
        n >>= 1;
    }
    return ret;
}

int main(){
    int n, k;
    scanf("%d%d", &n,&k);
    vector <int> a(n);
    int x = 0, y = 0, z = 0;
    for(int i = 0; i < n; i++){
        scanf("%d", &a[i]);
        x += a[i] == 0;
    }
    y = n - x;
    for(int i = 0; i < x; i++){
        z += a[i] == 0;
    }
    if(x > y){//如果0的个数大于1的个数，可以反向处理数组
        swap(x, y);
        z = 0;
        for(int i = n-1; i >= n - x; i--)
            z += a[i] == 1;
    }
    Matrix base;
    base.init();
    for(int i = 0; i <= x; i++){
        base.a[i][i] = x * (x - 1) / 2 + y * (y - 1) / 2 + i * (x - i) + (x - i) * (y - x + i);
        if(i >= 1) base.a[i][i - 1] = (x - i + 1) * (x - i + 1);
        if(i < x) base.a[i][i + 1] = (i + 1) * (y - x + i + 1);
    }
    base = qpow(base, k);
    LL ans = 0;
    for(int i = 0; i <= x; i++){
        ans = (ans + base.a[i][z]) % mod;
    }
    ans = base.a[x][z] * qsm(ans, mod - 2) % mod;
    printf("%lld\n", ans);
    return 0;
}
```



